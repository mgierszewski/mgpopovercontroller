//
//  MGPopoverControllerItemsViewController.h
//  MGPopoverController
//
//  Created by Maciek Gierszewski on 05.02.2015.
//  Copyright (c) 2015 Maciek Gierszewski. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MGPopoverControllerItem.h"
#import "MGAppearance.h"

@class MGItemsTableViewController;
@protocol MGItemsTableViewControllerDelegate <NSObject>
- (void)itemsTableViewController:(MGItemsTableViewController *)controller didPickItem:(MGPopoverControllerItem *)item;
@end

@interface MGItemsTableViewController : UITableViewController<MGAppearance>
@property (nonatomic, weak) id<MGItemsTableViewControllerDelegate> delegate;
@property (nonatomic, readonly) NSArray *items;

- (instancetype)initWithItems:(NSArray *)items;
@end
