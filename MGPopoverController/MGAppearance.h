//
//  MGAppearance.h
//  MGPopoverController
//
//  Created by Maciek Gierszewski on 06.02.2015.
//  Copyright (c) 2015 Maciek Gierszewski. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MGPopoverController.h"

@protocol MGAppearance <NSObject>
- (void)updateAppearanceInPopoverController:(MGPopoverController *)popoverController;
@end
