//
//  MGPopoverController.m
//  MGPopoverController
//
//  Created by Maciek Gierszewski on 05.02.2015.
//  Copyright (c) 2015 Maciek Gierszewski. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MGItemsTableViewController.h"
#import "MGPopoverRootViewController.h"
#import "MGPopoverController.h"
#import "MGBackgroundView.h"
#import "MGContainerView.h"
#import "MGArrowView.h"
#import "MGItemTableViewCell.h"
#import <objc/runtime.h>

// APPEARANCE
@interface MGPopoverController (UIAppearance_Private)
- (instancetype)initAppearance;
@end

// PRIVATE
@interface MGPopoverController () <MGPopoverRootViewControllerDelegate, MGItemsTableViewControllerDelegate>
+ (instancetype)sharedInstance;

@property (nonatomic, strong) UIWindow *window;
@property (nonatomic, strong) MGPopoverRootViewController *rootViewController;

- (void)updateAppearance;
@end

@implementation MGPopoverController

#pragma mark - Singleton

+ (id)allocWithZone:(NSZone *)zone
{
    return [self sharedInstance];
}

- (id)copyWithZone:(NSZone *)zone
{
    return self;
}

+ (instancetype)sharedInstance
{
    static MGPopoverController *popoverInstance = nil;
    static dispatch_once_t popoverInstanceOnceToken;
    dispatch_once(&popoverInstanceOnceToken, ^{
        popoverInstance = [[super allocWithZone:0] init];
    });
    
    return popoverInstance;
}

#pragma mark - Appearance

+ (instancetype)appearance
{
    static MGPopoverController *appearanceInstance = nil;
    static dispatch_once_t appearanceInstanceOnceToken;
    dispatch_once(&appearanceInstanceOnceToken, ^{
        appearanceInstance = [[super allocWithZone:0] initAppearance];
    });
    
    return appearanceInstance;
}

+ (instancetype)appearanceWhenContainedIn:(Class<UIAppearanceContainer>)ContainerClass, ...
{
    return nil;//[self appearance];
}

+ (instancetype)appearanceForTraitCollection:(UITraitCollection *)trait
{
    return nil;//[self appearance];
}

+ (instancetype)appearanceForTraitCollection:(UITraitCollection *)trait whenContainedIn:(Class<UIAppearanceContainer>)ContainerClass, ...
{
    return nil;//[self appearance];
}

#pragma mark - Constructor

- (instancetype)initAppearance
{
    self = [super init];
    if (self)
    {
        // Default values
        _backgroundColor = [UIColor whiteColor];
        _foregroundColor = [UIColor blackColor];
        _windowColor = [[UIColor blackColor] colorWithAlphaComponent:0.7f];
        _selectionBackgroundColor = [UIColor redColor];
        _selectionForegroundColor = [UIColor whiteColor];
        _itemTitleFont = [UIFont systemFontOfSize:14.0f];
    }
    return self;
}

- (instancetype)initWithItems:(NSArray *)itemsArray
{
    self = [super init];
    if (self && self != [MGPopoverController appearance])
    {
        MGItemsTableViewController *itemsViewController = [[MGItemsTableViewController alloc] initWithItems:itemsArray];
        itemsViewController.delegate = self;
        
        MGPopoverRootViewController *rootViewController = [[MGPopoverRootViewController alloc] initWithContentViewController:itemsViewController];
        rootViewController.delegate = self;
        
        UIWindow *window = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
        window.opaque = NO;
        
        self.rootViewController = rootViewController;
        self.window = window;
        
        self.backgroundColor = [[MGPopoverController appearance] backgroundColor];
        self.foregroundColor = [[MGPopoverController appearance] foregroundColor];
        self.windowColor = [[MGPopoverController appearance] windowColor];
        self.selectionBackgroundColor = [[MGPopoverController appearance] selectionBackgroundColor];
        self.selectionForegroundColor = [[MGPopoverController appearance] selectionForegroundColor];
        self.itemTitleFont = [[MGPopoverController appearance] itemTitleFont];
    }
    return self;
}

#pragma mark - Setter

- (void)setWindow:(UIWindow *)window
{
    if (self.window && self.window != window)
    {
        [self.window resignKeyWindow];
        [self.window setHidden:YES];
    }
    
    _window = window;
}

- (void)setForegroundColor:(UIColor *)foregroundColor
{
    _foregroundColor = foregroundColor;
    if (self == [MGPopoverController appearance])
    {
        [[MGPopoverController sharedInstance] setForegroundColor:foregroundColor];
    }
}

- (void)setBackgroundColor:(UIColor *)backgroundColor
{
    _backgroundColor = backgroundColor;
    if (self == [MGPopoverController appearance])
    {
        [[MGPopoverController sharedInstance] setBackgroundColor:backgroundColor];
    }
}

- (void)setSelectionBackgroundColor:(UIColor *)selectionColor
{
    _selectionBackgroundColor = selectionColor;
    if (self == [MGPopoverController appearance])
    {
        [[MGPopoverController sharedInstance] setSelectionBackgroundColor:selectionColor];
    }
}

- (void)setItemTitleFont:(UIFont *)itemTitleFont
{
    _itemTitleFont = itemTitleFont;
    if (self == [MGPopoverController appearance])
    {
        [[MGPopoverController sharedInstance] setItemTitleFont:itemTitleFont];
    }
}

- (void)setWindowColor:(UIColor *)windowColor
{
    _windowColor = windowColor;
    if (self == [MGPopoverController appearance])
    {
        [[MGPopoverController sharedInstance] setWindowColor:windowColor];
    }
}

- (void)setSelectionForegroundColor:(UIColor *)selectionForegroundColor
{
    _selectionForegroundColor = selectionForegroundColor;
    if (self == [MGPopoverController appearance])
    {
        [[MGPopoverController sharedInstance] setSelectionForegroundColor:selectionForegroundColor];
    }
}

- (void)updateAppearance
{
    [self.rootViewController updateAppearanceInPopoverController:self];
    
    if ([self.rootViewController.contentViewController conformsToProtocol:@protocol(MGAppearance)])
    {
        [(id<MGAppearance>)self.rootViewController.contentViewController updateAppearanceInPopoverController:self];
    }
}

#pragma mark - MGPopoverControllerItemsViewControllerDelegate

- (void)itemsTableViewController:(MGItemsTableViewController *)controller didPickItem:(MGPopoverControllerItem *)item
{
    if ([self.delegate respondsToSelector:@selector(popoverController:didPickItem:itemIndex:)])
    {
        NSInteger itemIndex = [controller.items indexOfObject:item];
        [self.delegate popoverController:self didPickItem:item itemIndex:itemIndex];
    }
    [self.rootViewController hideAnimated:YES];
}

#pragma mark - MGPopoverViewControllerDelegate

- (void)popoverRootViewControllerDidHide:(MGPopoverRootViewController *)controller
{
    if ([self.delegate respondsToSelector:@selector(popoverControllerDidHide:)])
    {
        [self.delegate popoverControllerDidHide:self];
    }
}

#pragma mark - Presentation

- (void)presentFromBarButtonItem:(UIBarButtonItem *)barButtonItem animated:(BOOL)animated
{
    if (self == [MGPopoverController appearance])
    {
        return;
    }
    
    self.rootViewController.originBarButtonItem = barButtonItem;
    
    [self.window setRootViewController:self.rootViewController];
    [self.window makeKeyAndVisible];
    
    [self updateAppearance];
    [self.rootViewController showAnimated:YES];
}

- (void)presentFromView:(UIView *)view animated:(BOOL)animated
{
    if (self == [MGPopoverController appearance])
    {
        return;
    }
    
    self.rootViewController.originView = view;
    
    [self.window setRootViewController:self.rootViewController];
    [self.window makeKeyAndVisible];
    
    [self updateAppearance];
    [self.rootViewController showAnimated:animated];
}

@end