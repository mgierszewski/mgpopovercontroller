//
//  MGContainerView.m
//  MGPopoverController
//
//  Created by Maciek Gierszewski on 05.02.2015.
//  Copyright (c) 2015 Maciek Gierszewski. All rights reserved.
//

#import "MGContainerView.h"

@interface MGContainerView ()
@property (nonatomic, weak) UIViewController *parentViewController;
@end

@implementation MGContainerView

+ (instancetype)containerViewWithParentViewController:(UIViewController *)viewController
{
    return [[MGContainerView alloc] initWithParentViewController:viewController];
}
- (instancetype)initWithParentViewController:(UIViewController *)viewController
{
    self = [super init];
    if (self)
    {
        self.parentViewController = viewController;
        
        super.clipsToBounds = YES;
    }
    return self;
}

#pragma mark - Layout

- (CGSize)intrinsicContentSize
{
    CGSize size = CGSizeZero;
    if ([self.contentViewController respondsToSelector:@selector(preferredContentSize)])
    {
        size = [self.contentViewController preferredContentSize];
    }
    else
    {
        size = [self.contentViewController.view intrinsicContentSize];
    }
    
    if (size.width > 0.0f && size.height > 0.0f)
    {
        return size;
    }
    
    return self.parentViewController.view.frame.size;
}

#pragma mark - Setter

- (void)setBackgroundColor:(UIColor *)backgroundColor
{
    [super setBackgroundColor:backgroundColor];
}

- (void)setContentViewController:(UIViewController *)contentViewController
{
    [self setContentViewController:contentViewController animated:NO];
}

- (void)setContentViewController:(UIViewController *)contentViewController animated:(BOOL)animated
{
    // animation block
    void (^animationBlock)(void) = ^{

        [self.contentViewController.view removeFromSuperview];
        if (contentViewController)
        {
            [self addSubview:contentViewController.view];
            contentViewController.view.translatesAutoresizingMaskIntoConstraints = NO;
            
            NSDictionary *views = @{@"view": contentViewController.view};
            [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[view]|" options:0 metrics:nil views:views]];
            [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[view]|" options:0 metrics:nil views:views]];
        }
    };
    
    // completion block
    void (^completionBlock)(BOOL) = ^(BOOL flag){
        [self.contentViewController removeFromParentViewController];
        
        [contentViewController didMoveToParentViewController:self.parentViewController];
        _contentViewController = contentViewController;
    };
    
    // execution
    [self.parentViewController addChildViewController:contentViewController];
    
    if (animated)
    {
        [UIView transitionWithView:self
                          duration:0.5
                           options:UIViewAnimationOptionTransitionCrossDissolve
                        animations:animationBlock
                        completion:completionBlock];
    }
    else
    {
        animationBlock();
        completionBlock(YES);
    }
}

@end
