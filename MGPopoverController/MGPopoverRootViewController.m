//
//  MGContainerViewController.m
//  MGPopoverController
//
//  Created by Maciek Gierszewski on 05.02.2015.
//  Copyright (c) 2015 Maciek Gierszewski. All rights reserved.
//

#import "MGPopoverRootViewController.h"
#import "MGContainerView.h"
#import "MGArrowView.h"
#import "MGBackgroundView.h"
#import "MGPopoverController.h"

@interface MGPopoverRootViewController ()
@property (nonatomic, strong) UIViewController *contentViewController;

@property (nonatomic, strong) MGContainerView *containerView;
@property (nonatomic, strong) MGArrowView *arrowView;
@property (nonatomic, strong) MGBackgroundView *backgroundView;
@property (nonatomic, strong) UITapGestureRecognizer *tapGestureRecognizer;

@property (nonatomic, strong) NSArray *constraints;

- (void)tapGestureRecognized:(UITapGestureRecognizer *)tapGestureRecognizer;
@end

@implementation MGPopoverRootViewController

- (instancetype)initWithContentViewController:(UIViewController *)contentViewController
{
    self = [super init];
    if (self)
    {
        _contentViewController = contentViewController;
    }
    return self;
}

- (void)loadView
{
    UIScreen *mainScreen = [UIScreen mainScreen];
    CGRect screenBounds = mainScreen.bounds;
    
    UIView *view = [[UIView alloc] initWithFrame:screenBounds];
    view.translatesAutoresizingMaskIntoConstraints = NO;
    view.backgroundColor = [UIColor clearColor];
    
    MGBackgroundView *backgroundView = [MGBackgroundView new];
    backgroundView.translatesAutoresizingMaskIntoConstraints = NO;
    backgroundView.backgroundColor = [UIColor colorWithWhite:0.0 alpha:0.7f];
    [view addSubview:backgroundView];
    
    MGArrowView *arrowView = [MGArrowView new];
    arrowView.translatesAutoresizingMaskIntoConstraints = NO;
    [view addSubview:arrowView];
    
    MGContainerView *containerView = [MGContainerView containerViewWithParentViewController:self];
    containerView.layer.cornerRadius = 4.0f;
    containerView.translatesAutoresizingMaskIntoConstraints = NO;
    [view addSubview:containerView];

    UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapGestureRecognized:)];
    tapGestureRecognizer.cancelsTouchesInView = NO;
    [view addGestureRecognizer:tapGestureRecognizer];
    
    self.tapGestureRecognizer = tapGestureRecognizer;
    self.backgroundView = backgroundView;
    self.containerView = containerView;
    self.arrowView = arrowView;
    
    self.view = view;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.containerView.contentViewController = self.contentViewController;
}

- (void)viewWillLayoutSubviews
{
    [super viewWillLayoutSubviews];
    
    [self.view setNeedsUpdateConstraints];
}

//- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
//{
//    [self.view setNeedsUpdateConstraints];
//}

#pragma mark - UIGestureRecognizer

- (void)tapGestureRecognized:(UITapGestureRecognizer *)tapGestureRecognizer
{
    if (tapGestureRecognizer.state == UIGestureRecognizerStateRecognized)
    {
        CGPoint touchLocation = [tapGestureRecognizer locationInView:self.view];
        if (!CGRectContainsPoint(self.containerView.frame, touchLocation))
        {
            [self hideAnimated:YES];
        }
    }
}

#pragma mark - Setters

- (void)setOriginBarButtonItem:(UIBarButtonItem *)originBarButtonItem
{
    _originBarButtonItem = originBarButtonItem;
    _originView = nil;
}

- (void)setOriginView:(UIView *)originView
{
    _originView = originView;
    _originBarButtonItem = nil;
}

#pragma mark - MGAppearance

- (void)updateAppearanceInPopoverController:(MGPopoverController *)popoverController
{
    self.arrowView.backgroundColor = [popoverController backgroundColor];
    self.containerView.backgroundColor = [popoverController backgroundColor];
    self.backgroundView.backgroundColor = [popoverController windowColor];
}

#pragma mark - Layout

- (void)updateViewConstraints
{
    [super updateViewConstraints];
    
    CGRect originRect = CGRectZero;
    if (self.originView)
    {
        if ([self.originView respondsToSelector:@selector(convertRect:toCoordinateSpace:)])
        {
            originRect = [self.originView convertRect:self.originView.bounds toCoordinateSpace:self.view];
        }
        else
        {
            originRect = [self.originView convertRect:self.originView.bounds toView:self.view];
        }
    }
    else if (self.originBarButtonItem)
    {
        UIView *view = [self.originBarButtonItem valueForKey:@"view"];
        if ([view respondsToSelector:@selector(convertRect:toCoordinateSpace:)])
        {
            originRect = [view convertRect:view.bounds toCoordinateSpace:self.view];
        }
        else
        {
            originRect = [view convertRect:view.bounds toView:self.view];
        }
        /*
         NSString *title = self.originBarButtonItem.title;
         UIImage *image = self.originBarButtonItem.image;
         UIView *originalCustomView = self.originBarButtonItem.customView;
         UIBarButtonItemStyle style = self.originBarButtonItem.style;
         
         UIView *view = originalCustomView;
         if (!view)
         {
         view = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 24.0f, 24.0f)];
         self.originBarButtonItem.customView = view;
         }
         
         originRect = [view convertRect:view.bounds toView:self.view];
         
         self.originBarButtonItem.style = style;
         self.originBarButtonItem.customView = originalCustomView;
         self.originBarButtonItem.image = image;
         self.originBarButtonItem.title = title;
         */
    }
    else
    {
        return;
    }
    
    // space
    CGFloat spaceAboveView = CGRectGetMaxY(originRect);
    CGFloat spaceBelowView = CGRectGetHeight(self.view.bounds) - CGRectGetMinY(originRect);
    
    NSDictionary *metrics = @{@"spaceBelow": @(spaceBelowView),
                              @"spaceAbove": @(spaceAboveView),
                              @"contentLeftMargin": @10,
                              @"contentRightMargin": @10,
                              @"arrowLeftMargin": @(10.0f + self.containerView.layer.cornerRadius),
                              @"arrowRightMargin": @(10.0f + self.containerView.layer.cornerRadius),
                              @"topMargin": @20,
                              @"bottomMargin": @20
                              };
    NSDictionary *views = @{@"containerView": self.containerView,
                            @"arrowView": self.arrowView,
                            @"backgroundView": self.backgroundView,
                            };
    
    // clear constraints
    if (self.constraints)
    {
        [self.view removeConstraints:self.constraints];
    }
    NSMutableArray *constraints = [NSMutableArray array];
    
    // background
    [constraints addObjectsFromArray:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[backgroundView]|" options:0 metrics:nil views:views]];
    [constraints addObjectsFromArray:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[backgroundView]|" options:0 metrics:nil views:views]];
    
    // vertical boundaries
    if (spaceAboveView > spaceBelowView)
    {
        self.arrowView.direction = MGArrowDirectionDown;
        [constraints addObjectsFromArray:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-(>=topMargin)-[containerView][arrowView]-(spaceBelow)-|" options:0 metrics:metrics views:views]];
    }
    else
    {
        self.arrowView.direction = MGArrowDirectionUp;
        [constraints addObjectsFromArray:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-spaceAbove-[arrowView][containerView]-(>=bottomMargin)-|" options:0 metrics:metrics views:views]];
    }
    
    // boundaries
    [constraints addObjectsFromArray:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-(>=arrowLeftMargin)-[arrowView]-(>=arrowRightMargin)-|" options:0 metrics:metrics views:views]];
    [constraints addObjectsFromArray:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-(>=contentLeftMargin)-[containerView]-(>=contentRightMargin)-|" options:0 metrics:metrics views:views]];

    // horizontal position
    NSLayoutConstraint *arrowCenterX = [NSLayoutConstraint constraintWithItem:self.arrowView attribute:NSLayoutAttributeCenterX
                                                                    relatedBy:NSLayoutRelationEqual
                                                                       toItem:self.view
                                                                    attribute:NSLayoutAttributeLeft
                                                                   multiplier:1.0f
                                                                     constant:CGRectGetMidX(originRect)];
    NSLayoutConstraint *containerCenterX = [NSLayoutConstraint constraintWithItem:self.containerView
                                                                        attribute:NSLayoutAttributeCenterX
                                                                        relatedBy:NSLayoutRelationEqual
                                                                           toItem:self.arrowView
                                                                        attribute:NSLayoutAttributeCenterX
                                                                       multiplier:1.0f
                                                                         constant:0.0f];
    
    arrowCenterX.priority = UILayoutPriorityDefaultHigh;
    containerCenterX.priority = UILayoutPriorityDefaultLow;
    
    [constraints addObject:arrowCenterX];
    [constraints addObject:containerCenterX];
    
    if (self.constraints)
    {
        [self.view removeConstraints:self.constraints];
    }
    
    self.constraints = [NSArray arrayWithArray:constraints];
    [self.view addConstraints:self.constraints];
}

#pragma mark - Animation

- (void)hideAnimated:(BOOL)animated
{
    [UIView animateWithDuration:0.2 animations:^{
        self.view.alpha = 0.0f;
    } completion:^(BOOL finished) {
        self.view.alpha = 1.0f;
        
        [self.view.window setHidden:YES];
        [self.view.window resignKeyWindow];
        [self.view.window setRootViewController:nil];
        
        if ([self.delegate respondsToSelector:@selector(popoverRootViewControllerDidHide:)])
        {
            [self.delegate popoverRootViewControllerDidHide:self];
        }
    }];
}

- (void)showAnimated:(BOOL)animated
{
    if (animated && self.containerView && self.arrowView)
    {
        // define constraints
        NSDictionary *views = @{@"containerView": self.containerView,
                                @"arrowView": self.arrowView
                                };
        
        NSMutableArray *smallSizeConstraints = [NSMutableArray array];
        [smallSizeConstraints addObjectsFromArray:[NSLayoutConstraint constraintsWithVisualFormat:@"H:[containerView(==0.0)]" options:0 metrics:nil views:views]];
        [smallSizeConstraints addObjectsFromArray:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[containerView(==0.0)]" options:0 metrics:nil views:views]];
        [smallSizeConstraints addObjectsFromArray:[NSLayoutConstraint constraintsWithVisualFormat:@"H:[arrowView(==0.0)]" options:0 metrics:nil views:views]];
//        [smallSizeConstraints addObjectsFromArray:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[arrowView(==0.0)]" options:0 metrics:nil views:views]];
 
        NSTimeInterval duration = 0.5;
        NSTimeInterval backgroundDuration = 0.2;
        
        // animate background
        self.backgroundView.alpha = 0.0f;
        [UIView animateWithDuration:backgroundDuration
                         animations:^{
                             self.backgroundView.alpha = 1.0f;
                         }
                         completion:^(BOOL finished) {
                         }];
        
        // animate content
        [self.view addConstraints:smallSizeConstraints];
        [self.view layoutIfNeeded];
        [self.view removeConstraints:smallSizeConstraints];
        
        [UIView animateWithDuration:duration
                              delay:backgroundDuration
             usingSpringWithDamping:25.0f
              initialSpringVelocity:25.0f
                            options:UIViewAnimationOptionBeginFromCurrentState
                         animations:^{
                             [self.view layoutIfNeeded];
                         }
                         completion:nil];
    }
}

@end
