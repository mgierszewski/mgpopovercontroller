//
//  MGPopoverControllerItem.h
//  MGPopoverController
//
//  Created by Maciek Gierszewski on 05.02.2015.
//  Copyright (c) 2015 Maciek Gierszewski. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

#define PopoverControllerItem(itemTitle, itemImage) [MGPopoverControllerItem popoverControllerItemWithTitle:(itemTitle) image:(itemImage)]

@interface MGPopoverControllerItem : NSObject
@property (nonatomic, readonly) NSString *title;
@property (nonatomic, readonly) UIImage *image;

- (instancetype)initWithTitle:(NSString *)title image:(UIImage *)image;
+ (instancetype)popoverControllerItemWithTitle:(NSString *)title image:(UIImage *)image;
@end
