//
//  MGArrowView.m
//  MGPopoverController
//
//  Created by Maciek Gierszewski on 05.02.2015.
//  Copyright (c) 2015 Maciek Gierszewski. All rights reserved.
//

#import "MGArrowView.h"

static CGFloat const ARROW_WIDTH = 20.0f;
static CGFloat const ARROW_HEIGHT = 12.0f;

@interface MGArrowView ()
@property (nonatomic, strong) UIColor *fillColor;
@end

@implementation MGArrowView

- (instancetype)init
{
    self = [super init];
    if (self)
    {
        self.clipsToBounds = NO;
        
        [self setContentCompressionResistancePriority:UILayoutPriorityRequired forAxis:UILayoutConstraintAxisHorizontal];
        [self setContentCompressionResistancePriority:UILayoutPriorityRequired forAxis:UILayoutConstraintAxisVertical];
        [self setContentHuggingPriority:UILayoutPriorityFittingSizeLevel forAxis:UILayoutConstraintAxisVertical];
        [self setContentHuggingPriority:UILayoutPriorityFittingSizeLevel forAxis:UILayoutConstraintAxisHorizontal];
    }
    return self;
}

- (void)drawRect:(CGRect)rect
{
    [super drawRect:rect];
    // Drawing code
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    [self.fillColor setFill];
    if (self.direction == MGArrowDirectionDown)
    {
        CGContextMoveToPoint(context, 0.0f, 0.0f);
        CGContextAddLineToPoint(context, CGRectGetWidth(rect) / 2.0f, CGRectGetHeight(rect));
        CGContextAddLineToPoint(context, CGRectGetWidth(rect), 0.0f);
    }
    else
    {
        CGContextMoveToPoint(context, 0.0f, CGRectGetHeight(rect));
        CGContextAddLineToPoint(context, CGRectGetWidth(rect), CGRectGetHeight(rect));
        CGContextAddLineToPoint(context, CGRectGetWidth(rect) / 2.0f, 0.0f);
    }
    CGContextClosePath(context);
    
    CGContextFillPath(context);
}

- (CGSize)intrinsicContentSize
{
    return CGSizeMake(ARROW_WIDTH, ARROW_HEIGHT);
}

#pragma mark - Setters

- (void)setDirection:(MGArrowDirection)direction
{
    _direction = direction;

    [self setNeedsDisplay];
}

- (void)setBackgroundColor:(UIColor *)backgroundColor
{
    [super setBackgroundColor:[UIColor clearColor]];
    self.fillColor = backgroundColor;
    
    [self setNeedsDisplay];
}

@end
