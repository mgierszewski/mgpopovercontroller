//
//  MGPopoverControllerItemCell.m
//  MGPopoverController
//
//  Created by Maciek Gierszewski on 05.02.2015.
//  Copyright (c) 2015 Maciek Gierszewski. All rights reserved.
//

#import "MGItemTableViewCell.h"
#import "MGPopoverController.h"

@implementation MGItemTableViewCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self)
    {
        self.backgroundColor = [UIColor clearColor];
//        self.textLabel.textColor = [[MGPopoverController new] foregroundColor];
        
        if ([self respondsToSelector:@selector(setLayoutMargins:)])
        {
            self.layoutMargins = UIEdgeInsetsZero;
            self.preservesSuperviewLayoutMargins = NO;
        }
    }
    return self;
}

@end
