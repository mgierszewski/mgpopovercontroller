//
//  MGContainerViewController.h
//  MGPopoverController
//
//  Created by Maciek Gierszewski on 05.02.2015.
//  Copyright (c) 2015 Maciek Gierszewski. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MGAppearance.h"

@class MGPopoverRootViewController;
@protocol MGPopoverRootViewControllerDelegate <NSObject>
- (void)popoverRootViewControllerDidHide:(MGPopoverRootViewController *)controller;
@end

@interface MGPopoverRootViewController : UIViewController<MGAppearance>
@property (nonatomic, readonly) UIViewController *contentViewController;

@property (nonatomic, weak) id<MGPopoverRootViewControllerDelegate> delegate;

// view or bar button item to track
@property (nonatomic, weak) UIView *originView;
@property (nonatomic, weak) UIBarButtonItem *originBarButtonItem;

- (instancetype)initWithContentViewController:(UIViewController *)contentViewController;

- (void)showAnimated:(BOOL)animated;
- (void)hideAnimated:(BOOL)animated;
@end
