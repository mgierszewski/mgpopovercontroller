//
//  MGPopoverControllerItemsViewController.m
//  MGPopoverController
//
//  Created by Maciek Gierszewski on 05.02.2015.
//  Copyright (c) 2015 Maciek Gierszewski. All rights reserved.
//

#import "MGItemsTableViewController.h"
#import "MGItemTableViewCell.h"
#import "MGPopoverControllerItem.h"
#import "MGPopoverController.h"

#define FOOTER_HEIGHT 1.0f/[[UIScreen mainScreen] scale]
#define MAX_WIDTH 300.0f

@interface MGItemsTableViewController ()
@property (nonatomic, strong) NSArray *items;
@end

@implementation MGItemsTableViewController

- (instancetype)initWithItems:(NSArray *)items
{
    self = [super initWithStyle:UITableViewStylePlain];
    if (self)
    {
        self.items = [items filteredArrayUsingPredicate:[NSPredicate predicateWithBlock:^BOOL(id evaluatedObject, NSDictionary *bindings) {
            return [evaluatedObject isKindOfClass:[MGPopoverControllerItem class]];
        }]];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.tableView registerClass:[MGItemTableViewCell class] forCellReuseIdentifier:@"Cell"];
    
    self.tableView.backgroundColor = [UIColor clearColor];
    self.tableView.rowHeight = 44.0f;
    self.tableView.separatorInset = UIEdgeInsetsMake(0.0f, 10.0f, 0.0f, 10.0f);
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 0.0f, FOOTER_HEIGHT)];
}

#pragma mark - Size

- (CGSize)preferredContentSize
{
    return CGSizeMake(MIN(MAX_WIDTH, self.tableView.contentSize.width), self.tableView.contentSize.height - CGRectGetHeight(self.tableView.tableFooterView.frame));
}

#pragma mark - MGAppearance

- (void)updateAppearanceInPopoverController:(MGPopoverController *)popoverController
{
    self.tableView.separatorColor = [popoverController.foregroundColor colorWithAlphaComponent:0.2f];
    [self.tableView reloadData];
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.items count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    MGPopoverControllerItem *item = self.items[indexPath.row];
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    
    cell.imageView.image = item.image;
    cell.textLabel.text = item.title;
    cell.textLabel.textColor = [[MGPopoverController appearance] foregroundColor];
    cell.textLabel.highlightedTextColor = [[MGPopoverController appearance] selectionForegroundColor];
    cell.textLabel.font = [[MGPopoverController appearance] itemTitleFont];
    
    cell.selectedBackgroundView = [UIView new];
    cell.selectedBackgroundView.backgroundColor = [[MGPopoverController appearance] selectionBackgroundColor];
    
    return cell;
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    id item = self.items[indexPath.row];
    if ([item isKindOfClass:[MGPopoverControllerItem class]] && [self.delegate respondsToSelector:@selector(itemsTableViewController:didPickItem:)])
    {
        [self.delegate itemsTableViewController:self didPickItem:(MGPopoverControllerItem *)item];
    }
}

@end
