//
//  ViewController.m
//  MGPopoverControllerTest
//
//  Created by Maciek Gierszewski on 05.02.2015.
//  Copyright (c) 2015 Maciek Gierszewski. All rights reserved.
//

#import "ViewController.h"
#import <MGPopoverController/MGPopoverController.h>

@interface ViewController () <MGPopoverControllerDelegate>
- (IBAction)buttonTouchedUp:(UIButton *)button;
- (IBAction)barButtnItemTouchedUp:(UIBarButtonItem *)barButtonItem;
@end

@implementation ViewController

- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self)
    {
        [[MGPopoverController appearance] setBackgroundColor:[[UIColor yellowColor] colorWithAlphaComponent:0.8f]];
        [[MGPopoverController appearance] setForegroundColor:[UIColor redColor]];
        [[MGPopoverController appearance] setSelectionBackgroundColor:[UIColor blueColor]];
        [[MGPopoverController appearance] setWindowColor:[[UIColor purpleColor] colorWithAlphaComponent:0.7f]];
    }
    return self;
}

- (IBAction)buttonTouchedUp:(UIButton *)button
{
    [[MGPopoverController appearance] presentFromView:button animated:YES];
    
    MGPopoverController *popoverController = [[MGPopoverController alloc] initWithItems:@[PopoverControllerItem(@"title", nil),
                                                                                          PopoverControllerItem(@"title 2", nil),
                                                                                          PopoverControllerItem(@"title 3", nil)]];
    [popoverController setDelegate:self];
    [popoverController presentFromView:button animated:YES];
}

- (IBAction)barButtnItemTouchedUp:(UIBarButtonItem *)barButtonItem
{
    MGPopoverController *popoverController = [[MGPopoverController alloc] initWithItems:@[PopoverControllerItem(@"title", nil),
                                                                                          PopoverControllerItem(@"title 2", nil),
                                                                                          PopoverControllerItem(@"title 3", nil)]];
    [popoverController setDelegate:self];
    [popoverController presentFromBarButtonItem:barButtonItem animated:YES];
}

#pragma mark - MGPopoverControllerDelegate

- (void)popoverControllerDidHide:(MGPopoverController *)controller
{
    controller.backgroundColor = [[UIColor greenColor] colorWithAlphaComponent:0.8f];
    
    [controller setDelegate:nil];
    [controller presentFromBarButtonItem:self.navigationItem.rightBarButtonItem animated:YES];
}

@end
