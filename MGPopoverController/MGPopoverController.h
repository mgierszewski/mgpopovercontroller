//
//  MGPopoverController.h
//  MGPopoverController
//
//  Created by Maciek Gierszewski on 05.02.2015.
//  Copyright (c) 2015 Maciek Gierszewski. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MGPopoverControllerItem.h"

@class MGPopoverController;
@protocol MGPopoverControllerDelegate <NSObject>

@optional
- (void)popoverController:(MGPopoverController *)popoverController didPickItem:(MGPopoverControllerItem *)item itemIndex:(NSInteger)index;
- (void)popoverControllerDidHide:(MGPopoverController *)controller;
@end

// MGPopoverController
@interface MGPopoverController : NSObject <UIAppearance>
@property (nonatomic, weak) id<MGPopoverControllerDelegate> delegate;

- (instancetype)initWithItems:(NSArray *)itemsArray;

- (void)presentFromView:(UIView *)view animated:(BOOL)animated;
- (void)presentFromBarButtonItem:(UIBarButtonItem *)barButtonItem animated:(BOOL)animated;

// Appearance
@property (nonatomic, strong) UIColor *foregroundColor UI_APPEARANCE_SELECTOR;
@property (nonatomic, strong) UIColor *backgroundColor UI_APPEARANCE_SELECTOR;
@property (nonatomic, strong) UIColor *selectionBackgroundColor UI_APPEARANCE_SELECTOR;
@property (nonatomic, strong) UIColor *selectionForegroundColor UI_APPEARANCE_SELECTOR;
@property (nonatomic, strong) UIColor *windowColor UI_APPEARANCE_SELECTOR;
@property (nonatomic, strong) UIFont *itemTitleFont UI_APPEARANCE_SELECTOR;
@end