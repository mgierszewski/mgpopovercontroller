//
//  MGPopoverControllerItem.m
//  MGPopoverController
//
//  Created by Maciek Gierszewski on 05.02.2015.
//  Copyright (c) 2015 Maciek Gierszewski. All rights reserved.
//

#import "MGPopoverControllerItem.h"

@interface MGPopoverControllerItem ()
@property (nonatomic, strong) UIImage *image;
@property (nonatomic, strong) NSString *title;
@end

@implementation MGPopoverControllerItem

- (instancetype)initWithTitle:(NSString *)title image:(UIImage *)image
{
    self = [super init];
    if (self)
    {
        self.image = image;
        self.title = title;
    }
    return self;
}

+ (instancetype)popoverControllerItemWithTitle:(NSString *)title image:(UIImage *)image
{
    return [[MGPopoverControllerItem alloc] initWithTitle:title image:image];
}

@end
