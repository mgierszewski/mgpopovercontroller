//
//  MGArrowView.h
//  MGPopoverController
//
//  Created by Maciek Gierszewski on 05.02.2015.
//  Copyright (c) 2015 Maciek Gierszewski. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, MGArrowDirection)
{
    MGArrowDirectionUp,
    MGArrowDirectionDown,
};

@interface MGArrowView : UIView
@property (nonatomic, assign) MGArrowDirection direction;
@end
